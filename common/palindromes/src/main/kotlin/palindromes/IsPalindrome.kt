package palindromes

fun isPalindrome(argument: Int) : Boolean {
    return isPalindrome(argument.toString())
}

fun isPalindrome(argument: Long) : Boolean {
    return isPalindrome(argument.toString())
}

fun isPalindrome(argument: String) : Boolean {
    val chars = argument.toCharArray().asList()
    return chars == chars.reversed()
}