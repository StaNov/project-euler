package palindromes

import kotlin.test.*

class IsPalindromeTest {
    @Test
    fun `One letter is palindrome`() {
        assertTrue(isPalindrome("a"))
    }

    @Test
    fun `Not a palindrome`() {
        assertFalse(isPalindrome("not a palindrome"))
    }

    @Test
    fun `Palindrome with a letter in between`() {
        assertTrue(isPalindrome("tacocat"))
    }

    @Test
    fun `Palindrome without a letter in between`() {
        assertTrue(isPalindrome("abba"))
    }

    @Test
    fun `Palindrome number`() {
        assertTrue(isPalindrome(123454321))
    }

    @Test
    fun `Not a palindrome number`() {
        assertFalse(isPalindrome(123456789))
    }

    @Test
    fun `Palindrome number Long`() {
        assertTrue(isPalindrome(123454321L))
    }

    @Test
    fun `Not a palindrome number Long`() {
        assertFalse(isPalindrome(123456789L))
    }
}