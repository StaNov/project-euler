package cached

import kotlin.test.Test
import kotlin.test.assertEquals

class CachedComputerTest {

    @Test
    fun `Constant computation gives good result`() {
        val computer = CachedComputer<Any, Int> { 1 }
        assertEquals(1, computer.compute(123))
    }

    @Test
    fun `Computation returning input returns input`() {
        val computer = CachedComputer { x: Int -> x }
        assertEquals(456, computer.compute(456))
    }

    @Test
    fun `Calling computer twice with the same argument, but computing function is called only once`() {
        val spy = ComputationSpy()
        val computation = { x: Int -> spy.compute(x) }
        val computer = CachedComputer(computation)
        computer.compute(123)
        computer.compute(123)
        assertEquals(listOf(123), spy.argumentsCalled)
    }

    @Test
    fun `Works also with strings`() {
        val computer = CachedComputer { string: String -> string.length }
        assertEquals(5, computer.compute("abcde"))
    }

    private class ComputationSpy() {
        private val _argumentsCalled = mutableListOf<Int>()

        fun compute(x: Int) : Int {
            _argumentsCalled.add(x)
            return x
        }

        val argumentsCalled : List<Int> get() {
            return _argumentsCalled.toList()
        }
    }
}