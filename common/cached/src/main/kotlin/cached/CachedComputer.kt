package cached

class CachedComputer<T, U>(private val computation: (T) -> U) {
    private val cache = HashMap<T, U>()

    fun compute(i: T): U {
        return cache.getOrPut(i) { computation(i) }
    }
}
