package hugeint

class HugeInt {

    private val digits: List<Byte>

    constructor(source: String) : this(source.map { it.toString().toByte() })

    constructor(sourceInt: Int) : this(sourceInt.toString())

    private constructor(digits: List<Byte>) {
        this.digits = digits.dropWhile { it == 0.toByte() }
    }

    override fun toString(): String {
        return digits.fold("") {a, b -> a + b}
    }

    operator fun plus(that: HugeInt): HugeInt {
        if (digits.size < that.digits.size)
            return that.plus(this)

        val thisDigitsReversed = reversedMutableWithAddedZero(digits)
        val thatDigitsReversed = reversedMutableWithAddedZero(that.digits)

        while (thisDigitsReversed.size > thatDigitsReversed.size)
            thatDigitsReversed.add(0)

        var overTen = false
        val resultDigitsReversed = thisDigitsReversed.zip(thatDigitsReversed, fun(a, b) : Byte {
            val sum = a + b + (if (overTen) 1 else 0)
            overTen = sum >= 10
            return (sum % 10).toByte()
        })

        return HugeInt(resultDigitsReversed.reversed())
    }

    private fun reversedMutableWithAddedZero(list: List<Byte>): MutableList<Byte> {
        val result = list.reversed().toMutableList()
        result.add(0)
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HugeInt

        if (digits != other.digits) return false

        return true
    }

    override fun hashCode(): Int {
        return digits.hashCode()
    }
}
