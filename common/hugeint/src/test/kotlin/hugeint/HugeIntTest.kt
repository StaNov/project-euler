package hugeint

import kotlin.test.Test
import kotlin.test.assertEquals

class HugeIntTest {
    @Test
    fun `Create HugeInt from Int`() {
        assertEquals("123", HugeInt(123).toString())
    }

    @Test
    fun `Create from String`() {
        assertEquals("456", HugeInt("456").toString())
    }

    @Test
    fun `Create from String with trailing zeroes`() {
        assertEquals("987", HugeInt("000987").toString())
    }

    @Test
    fun `Create HugeInt from long String`() {
        val input = "123456789".repeat(20)
        assertEquals(input, HugeInt(input).toString())
    }

    @Test
    fun `One plus one`() {
        assertPlus(1, 1)
    }

    @Test
    fun `One plus two`() {
        assertPlus(1, 2)
    }

    @Test
    fun `Adding over ten`() {
        assertPlus(8, 7)
    }

    @Test
    fun `Adding big numbers over ten`() {
        assertPlus(123456, 129812)
    }

    @Test
    fun `Adding with different length`() {
        assertPlus(1234, 1)
    }

    @Test
    fun `Adding with different length, second is longer`() {
        assertPlus(1, 1234)
    }

    private fun assertPlus(a: Int, b: Int) {
        assertEquals(HugeInt(a + b), HugeInt(a) + HugeInt(b))
    }
}