package primes

import java.lang.IllegalArgumentException
import kotlin.test.*

class IsPrimeTest {
    @Test(expected = IllegalArgumentException::class)
    fun `Zero is not testable`() {
        isPrime(0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `Negative number is not testable`() {
        isPrime(-1)
    }

    @Test
    fun `One is not prime`() {
        assertFalse(isPrime(1))
    }

    @Test
    fun `Two is prime`() {
        assertTrue(isPrime(2))
    }

    @Test
    fun `Three is prime`() {
        assertTrue(isPrime(3))
    }

    @Test
    fun `Four is not prime`() {
        assertFalse(isPrime(4))
    }

    @Test
    fun `Nine is not prime`() {
        assertFalse(isPrime(9))
    }

    @Test
    fun `Bell's prime is prime`() {
        assertTrue(isPrime(27644437))
    }

    @Test
    fun `16127 * 16127 is not a prime`() {
        assertFalse(isPrime(260080129))
    }
}