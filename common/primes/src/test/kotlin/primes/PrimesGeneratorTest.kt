package primes

import kotlin.test.Test
import kotlin.test.assertEquals

class PrimesGeneratorTest {
    @Test
    fun `First 10 primes are correct`() {
        val expected = listOf<Long>(2, 3, 5, 7, 11, 13, 17, 19, 23, 29)
        val actual = infinitePrimesSequence().take(10).toList()

        assertEquals(expected, actual)
    }
}