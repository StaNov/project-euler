package primes

import kotlin.math.sqrt

private val cachedPrimes = mutableSetOf<Long>()
private val cachedNotPrimes = mutableSetOf<Long>()

fun isPrime(argument: Long) : Boolean {
    if (argument in cachedPrimes)
        return true

    if (argument in cachedNotPrimes)
        return false

    val result = isPrimeInternal(argument)

    if (result)
        cachedPrimes.add(argument)
    else
        cachedNotPrimes.add(argument)

    return result
}

private fun isPrimeInternal(argument: Long) : Boolean {
    require(argument > 0) { "Numbers below zero are not testable for being primes." }

    if (argument == 1L)
        return false

    if (argument == 2L || argument == 3L)
        return true

    if (argument % 2 == 0L)
        return false

    val upperBound = sqrt(argument.toDouble()).toInt()

    for (divisor in 3 .. upperBound step 2)
        if (argument % divisor == 0L)
            return false

    return true
}