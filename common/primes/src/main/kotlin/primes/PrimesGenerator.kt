package primes

fun infinitePrimesSequence() : Sequence<Long>{
    return sequence {
        yield(2L)

        for (value in 3..Long.MAX_VALUE step 2)
            if (isPrime(value))
                yield(value)
    }
}