package common

import kotlin.math.pow

fun Int.pow(exponent: Int): Int {
    return toDouble().pow(exponent).toInt()
}

fun Long.pow(exponent: Int): Long {
    return toDouble().pow(exponent).toLong()
}