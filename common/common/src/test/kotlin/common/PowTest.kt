package common

import kotlin.test.Test
import kotlin.test.assertEquals

class PowTest {
    @Test
    fun `Int pow`() {
        assertEquals(1000, 10.pow(3))
    }

    @Test
    fun `Long pow`() {
        assertEquals(10000000000L, 100000L.pow(2))
    }
}
