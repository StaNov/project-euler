package fibonacci

import kotlin.test.Test
import kotlin.test.assertEquals

class FibonacciGeneratorTest {
    @Test
    fun `First 10 members of the Fibonacci sequence are correct`() {
        val expected = listOf(1, 2, 3, 5, 8, 13, 21, 34, 55, 89)
        val actual = infiniteFibonacciSequence(1, 2, {i, j -> i + j})
                .take(10)
                .toList()

        assertEquals(expected, actual)
    }
}