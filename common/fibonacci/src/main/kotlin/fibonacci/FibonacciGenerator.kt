package fibonacci

fun infiniteFibonacciSequence(firstNumber: Long, secondNumber: Long) : Sequence<Long> {
    return infiniteFibonacciSequence(firstNumber, secondNumber) { i, j -> i + j }
}

fun <T : Any> infiniteFibonacciSequence(firstNumber: T, secondNumber: T, plus: (T, T) -> T) : Sequence<T> {
    var currentNumber1 = firstNumber
    var currentNumber2 = secondNumber

    return sequence {
        yield(currentNumber1)
        yield(currentNumber2)

        while (true) {
            val newNumber = plus(currentNumber1, currentNumber2)

            currentNumber1 = currentNumber2
            currentNumber2 = newNumber

            yield(newNumber)
        }
    }
}