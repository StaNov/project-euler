package minmax

import kotlin.test.Test
import kotlin.test.assertEquals

class KeyValueMinMaxKeeperTest {

    @Test
    fun `Adding one value, returns the inserted key`() {
        val keeper = KeyValueMinMaxKeeper<String, Int>()
        keeper.add("aaa", 1)
        assertEquals("aaa", keeper.minKey)
        assertEquals("aaa", keeper.maxKey)
    }

    @Test
    fun `Adding three values`() {
        val keeper = KeyValueMinMaxKeeper<String, Int>()
        keeper.add("Highest", 5)
        keeper.add("Lowest", 1)
        keeper.add("Middle", 3)
        assertEquals("Lowest", keeper.minKey)
        assertEquals("Highest", keeper.maxKey)
    }
}