package minmax

import kotlin.test.Test
import kotlin.test.assertEquals

class MinMaxKeeperTest {
    @Test
    fun `One number`() {
        val keeper = MinMaxKeeper<Int>()
        keeper.add(23)
        assertEquals(23, keeper.min)
        assertEquals(23, keeper.max)
    }

    @Test
    fun `Two numbers`() {
        val keeper = MinMaxKeeper<Int>()
        keeper.add(48)
        keeper.add(23)
        assertEquals(23, keeper.min)
        assertEquals(48, keeper.max)
    }

    @Test
    fun `Two strings`() {
        val keeper = MinMaxKeeper<String>()
        keeper.add("bbb")
        keeper.add("aaa")
        assertEquals("aaa", keeper.min)
        assertEquals("bbb", keeper.max)
    }

    @Test(expected = IllegalStateException::class)
    fun `Called min before anything was added`() {
        MinMaxKeeper<String>().min
    }

    @Test(expected = IllegalStateException::class)
    fun `Called max before anything was added`() {
        MinMaxKeeper<String>().max
    }
}
