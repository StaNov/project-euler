package minmax

open class MinMaxKeeper<T : Comparable<T>> {
    private var currentMin: T? = null
    private var currentMax: T? = null

    fun add(i: T) {
        if (currentMax == null || i > currentMax!!)
            currentMax = i

        if (currentMin == null || i < currentMin!!)
            currentMin = i
    }


    val min: T get() {
        checkNotNull(currentMin) { "Something has to be added before calling min!" }
        return currentMin!!
    }
    val max: T get() {
        checkNotNull(currentMin) { "Something has to be added before calling max!" }
        return currentMax!!
    }
}
