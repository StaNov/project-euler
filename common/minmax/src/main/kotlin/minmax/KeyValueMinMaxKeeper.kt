package minmax

class KeyValueMinMaxKeeper<K, V: Comparable<V>> : MinMaxKeeper<TupleComparableByValue<K, V>>() {

    fun add(key: K, value: V) {
        add(TupleComparableByValue(key, value))
    }

    val maxKey: K get() {
        return max.key
    }

    val minKey: K get() {
        return min.key
    }
}

class TupleComparableByValue<K, V: Comparable<V>>(val key: K, private val value: V)
    : Comparable<TupleComparableByValue<K, V>> {

    override fun compareTo(other: TupleComparableByValue<K, V>): Int {
        return value.compareTo(other.value)
    }

}