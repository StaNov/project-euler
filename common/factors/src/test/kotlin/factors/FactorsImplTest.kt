package factors

import kotlin.test.Test
import kotlin.test.assertEquals

class FactorsImplTest {
    @Test
    fun `getList returns passed list, even if the passed list was modified afterwards`() {
        val inputList = mutableListOf(1L, 2, 3, 4)
        val factors = FactorsImpl(inputList)
        inputList.add(5)

        assertEquals(listOf(1L, 2, 3, 4), factors.getList())
    }

    @Test
    fun getMap() {
        val inputList = listOf(2L, 2, 3, 3, 5, 7, 11, 11, 11)
        val factors = FactorsImpl(inputList)

        assertEquals(mapOf(
                Pair(2L, 2),
                Pair(3L, 2),
                Pair(5L, 1),
                Pair(7L, 1),
                Pair(11L, 3)
        ), factors.getMap())
    }
}