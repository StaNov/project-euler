package factors

import kotlin.test.Test
import kotlin.test.assertEquals

class PrimeFactorsGeneratorTest {
    @Test
    fun `One has no prime factor`() {
        assertEqualsList(listOf(), 1)
    }

    @Test
    fun `2 = 2`() {
        assertEqualsList(listOf(2L), 2)
    }

    @Test
    fun `3 = 3`() {
        assertEqualsList(listOf(3L), 3)
    }

    @Test
    fun `4 = 2 * 2`() {
        assertEqualsList(listOf(2L, 2L), 4)
    }

    @Test
    fun `5 = 5`() {
        assertEqualsList(listOf(5L), 5)
    }

    @Test
    fun `6 = 2 * 3`() {
        assertEqualsList(listOf(2L, 3L), 6)
    }

    @Test
    fun `7 = 7`() {
        assertEqualsList(listOf(7L), 7)
    }

    @Test
    fun `8 = 2 * 2 * 2`() {
        assertEqualsList(listOf(2L, 2L, 2L), 8)
    }

    @Test
    fun `9 = 3`() {
        assertEqualsList(listOf(3L, 3L), 9)
    }

    @Test
    fun `Big number`() {
        assertEqualsList(listOf(2L, 2, 3, 3, 3, 5, 19, 29), 2 * 2 * 3 * 3 * 3 * 5 * 19 * 29)
    }

    private fun assertEqualsList(expected: List<Long>, factorsOf: Long) {
        assertEquals(expected, primeFactorsOf(factorsOf).getList())
    }
}