package factors

import primes.infinitePrimesSequence

fun primeFactorsOf(argument: Long) : Factors {
    if (argument <= 1)
        return FactorsImpl()

    var arg = argument
    val result = mutableListOf<Long>()

    for (factor in infinitePrimesSequence()) {
        while (arg % factor == 0L) {
            result.add(factor)
            arg /= factor
        }

        if (arg == 1L)
            return FactorsImpl(result)
    }

    throw IllegalStateException("Unreachable code, since infinitePrimesSequence is infinite.")
}

