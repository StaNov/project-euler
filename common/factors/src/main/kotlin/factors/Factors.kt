package factors

interface Factors {
    /**
     * Returns list of factors, sorted from smallest to largest.
     */
    fun getList(): List<Long>

    /**
     * Returns map of factors with their counts.
     *
     * Eg, for 120:
     *
     * {
     *      2: 3
     *      3: 1
     *      5: 1
     * }
     */
    fun getMap(): Map<Long, Int>
}