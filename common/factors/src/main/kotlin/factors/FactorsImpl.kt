package factors

internal class FactorsImpl(factors: List<Long> = listOf()) : Factors {

    private val factors: List<Long> = factors.toList()

    override fun getList(): List<Long> {
        return factors
    }

    override fun getMap(): Map<Long, Int> {
        val result = mutableMapOf<Long, Int>()

        for (factor in factors)
            result.compute(factor) { _, count -> (count ?: 0) + 1 }

        return result.toMap()
    }

}