package problem007

import primes.infinitePrimesSequence

class Solver {
    fun nthPrime(n: Int): Long {
        return infinitePrimesSequence().elementAt(n-1)
    }

}
