package problem007

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun sample() {
        assertEquals(13, solver.nthPrime(6))
    }

    @Test
    fun full() {
        assertEquals(104743, solver.nthPrime(10001))
    }
}
