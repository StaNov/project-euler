package problem003

import factors.primeFactorsOf

class Solver {
    fun largestPrimeFactorOf(argument: Long): Long {
        require(argument >= 2) { "Argument must be at least 2." }
        return primeFactorsOf(argument).getList().last()
    }

}
