package problem003

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test(expected = IllegalArgumentException::class)
    fun `One has no prime factor`() {
        solver.largestPrimeFactorOf(1)
    }

    @Test
    fun `2 = 2`() {
        assertEquals(2, solver.largestPrimeFactorOf(2))
    }

    @Test
    fun `3 = 3`() {
        assertEquals(3, solver.largestPrimeFactorOf(3))
    }

    @Test
    fun `4 = 2 * 2`() {
        assertEquals(2, solver.largestPrimeFactorOf(4))
    }

    @Test
    fun `5 = 5`() {
        assertEquals(5, solver.largestPrimeFactorOf(5))
    }

    @Test
    fun `6 = 2 * 3`() {
        assertEquals(3, solver.largestPrimeFactorOf(6))
    }

    @Test
    fun `7 = 7`() {
        assertEquals(7, solver.largestPrimeFactorOf(7))
    }

    @Test
    fun `8 = 2 * 2 * 2`() {
        assertEquals(2, solver.largestPrimeFactorOf(8))
    }

    @Test
    fun `9 = 3`() {
        assertEquals(3, solver.largestPrimeFactorOf(9))
    }

    @Test
    fun sample() {
        assertEquals(29, solver.largestPrimeFactorOf(13195))
    }

    @Test
    fun full() {
        assertEquals(6857, solver.largestPrimeFactorOf(600851475143))
    }
}
