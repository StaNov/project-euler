package problem002

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `Below 0 - nothing, therefore sum is zero`() {
        assertEquals(0, solver.sumOfEvenFibonacciNumbersBelow(0))
    }

    @Test
    fun `Below 3 - 2`() {
        assertEquals(2, solver.sumOfEvenFibonacciNumbersBelow(3))
    }

    @Test
    fun `Below 10 - 2 + 8 = 10`() {
        assertEquals(10, solver.sumOfEvenFibonacciNumbersBelow(10))
    }

    @Test
    fun sample() {
        assertEquals(44, solver.sumOfEvenFibonacciNumbersBelow(50))
    }

    @Test
    fun full() {
        assertEquals(4613732, solver.sumOfEvenFibonacciNumbersBelow(4_000_000))
    }
}
