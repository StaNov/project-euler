package problem002

class Solver {
    fun sumOfEvenFibonacciNumbersBelow(limit: Long): Long {
        return fibonacci.infiniteFibonacciSequence(0, 1)
                .takeWhile { it < limit }
                .filter { it % 2 == 0L }
                .sum()
    }
}
