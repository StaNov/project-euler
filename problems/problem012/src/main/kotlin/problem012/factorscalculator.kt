package problem012

import factors.primeFactorsOf
import java.util.*

fun factorsOf(x: Long) : Set<Long> {
    val result : SortedSet<Long> = TreeSet<Long>()
    result.add(1)

    for (primeFactor in primeFactorsOf(x).getList()) {
        val newValues = result
                .map { it * primeFactor }
                .filter { it <= x }
        result.addAll(newValues)
    }

    return result.toSet()
}