package problem012

fun triangleNumbersSequence() : Sequence<Long> {
    return sequence {
        var currentNumber = 0L
        for (i in 1..Long.MAX_VALUE) {
            currentNumber += i
            yield(currentNumber)
        }
    }
}