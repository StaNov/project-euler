package problem012

class Solver {
    fun firstTriangleNumberWithNDivisors(divisorsCount: Int): Long {
        return triangleNumbersSequence()
                .first { factorsOf(it).size > divisorsCount }

    }
}
