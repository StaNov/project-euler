package problem012

import kotlin.test.Test
import kotlin.test.assertEquals

class TriangleNumbersSequenceTest {

    @Test
    fun sequenceStartsWithCorrectTriangleNumbers() {
        assertEquals(
                listOf(1L, 3, 6, 10, 15, 21, 28, 36, 45, 55),
                triangleNumbersSequence().take(10).toList())
    }
}