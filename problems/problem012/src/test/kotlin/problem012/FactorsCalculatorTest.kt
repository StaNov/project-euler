package problem012

import kotlin.test.Test
import kotlin.test.assertEquals

class FactorsCalculatorTest {

    @Test
    fun `One is divisible by one`() {
        assertEquals(setOf(1L), factorsOf(1))
    }

    @Test
    fun `Two is divisible by one and two`() {
        assertEquals(setOf(1L, 2), factorsOf(2))
    }

    @Test
    fun `Four is divisible by one, two and four`() {
        assertEquals(setOf(1L, 2, 4), factorsOf(4))
    }

    @Test
    fun `Six is divisible by one, two, three and six`() {
        assertEquals(setOf(1L, 2, 3, 6), factorsOf(6))
    }

    @Test
    fun `Eight is divisible by one, two, four and eight`() {
        assertEquals(setOf(1L, 2, 4, 8), factorsOf(8))
    }

    @Test
    fun `Twenty-eight`() {
        assertEquals(setOf(1L, 2, 4, 7, 14, 28), factorsOf(28))
    }
}