package problem012

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `Over zero`() {
        assertEquals(1, solver.firstTriangleNumberWithNDivisors(0))
    }

    @Test
    fun `Over one`() {
        assertEquals(3, solver.firstTriangleNumberWithNDivisors(1))
    }

    @Test
    fun `Over two`() {
        assertEquals(6, solver.firstTriangleNumberWithNDivisors(2))
    }

    @Test
    fun `Over three`() {
        assertEquals(6, solver.firstTriangleNumberWithNDivisors(3))
    }

    @Test
    fun sample() {
        assertEquals(28, solver.firstTriangleNumberWithNDivisors(5))
    }

    @Test
    fun full() {
        assertEquals(76576500, solver.firstTriangleNumberWithNDivisors(500))
    }
}
