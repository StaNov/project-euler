package problem005

import common.pow
import factors.primeFactorsOf
import kotlin.math.max

class Solver {
    fun smallestNumberDivisibleByNumbersUpTo(upperBound: Int): Long {
        val factorCounts = mutableMapOf<Long, Int>()

        for (currentNumber in 1..upperBound) {
            primeFactorsOf(currentNumber.toLong()).getMap().forEach {
                val factor = it.key
                val count = it.value
                factorCounts.compute(factor) {
                    _, oldCount -> max(count, oldCount ?: 0)
                }
            }
        }

        var result = 1L
        factorCounts.forEach {
            (factor, count) -> result *= factor.pow(count)
        }
        return result
    }
}
