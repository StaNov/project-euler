package problem005

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun one() {
        assertEquals(1, solver.smallestNumberDivisibleByNumbersUpTo(1))
    }

    @Test
    fun two() {
        assertEquals(2, solver.smallestNumberDivisibleByNumbersUpTo(2))
    }

    @Test
    fun three() {
        assertEquals(6, solver.smallestNumberDivisibleByNumbersUpTo(3))
    }

    @Test
    fun four() {
        assertEquals(12, solver.smallestNumberDivisibleByNumbersUpTo(4))
    }

    @Test
    fun sample() {
        assertEquals(2520, solver.smallestNumberDivisibleByNumbersUpTo(10))
    }

    @Test
    fun full() {
        assertEquals(232792560, solver.smallestNumberDivisibleByNumbersUpTo(20))
    }
}
