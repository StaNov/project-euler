package problem010

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun sample() {
        assertEquals(17, solver.sumOfPrimesBelow(10))
    }

    @Test
    fun full() {
        assertEquals(142913828922, solver.sumOfPrimesBelow(2_000_000))
    }
}
