package problem010

import primes.infinitePrimesSequence

class Solver {
    fun sumOfPrimesBelow(limit: Int): Long {
        return infinitePrimesSequence()
                .takeWhile { it < limit }
                .sum()
    }
}
