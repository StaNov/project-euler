package problem008

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `One digit product in one digit number`() {
        assertEquals(
                7,
                solver.greatestProductOfConsecutiveDigits(1, "7")
        )
    }

    @Test
    fun `One digit product in two digit number`() {
        assertEquals(
                8,
                solver.greatestProductOfConsecutiveDigits(1, "78")
        )
    }

    @Test
    fun `One digit product in multi digit number`() {
        assertEquals(
                4,
                solver.greatestProductOfConsecutiveDigits(1, "11121312412312")
        )
    }

    @Test
    fun `Two digit product`() {
        assertEquals(
                3 * 4,
                solver.greatestProductOfConsecutiveDigits(2, "191121342412312")
        )
    }

    @Test
    fun `One digit product`() {
        assertEquals(9, solver.greatestProductOfConsecutiveDigits(1))
    }

    @Test
    fun `Two digits product`() {
        assertEquals(9 * 9, solver.greatestProductOfConsecutiveDigits(2))
    }

    @Test
    fun sample() {
        assertEquals(5832, solver.greatestProductOfConsecutiveDigits(4))
    }

    @Test
    fun full() {
        assertEquals(23514624000, solver.greatestProductOfConsecutiveDigits(13))
    }
}
