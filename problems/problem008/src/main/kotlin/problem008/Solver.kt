package problem008

import minmax.MinMaxKeeper

class Solver {
    fun greatestProductOfConsecutiveDigits(
            digits: Int,
            inputNumber: String = INPUT_NUMBER
    ): Long {
        val minMaxKeeper = MinMaxKeeper<Long>()

        for (startingIndex in 0..inputNumber.length - digits) {
            var currentProduct = 1L
            for (i in startingIndex until startingIndex + digits)
                currentProduct *= inputNumber[i].toString().toLong()
            minMaxKeeper.add(currentProduct)
        }

        return minMaxKeeper.max
    }
}
