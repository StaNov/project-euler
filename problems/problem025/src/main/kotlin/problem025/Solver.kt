package problem025

import fibonacci.infiniteFibonacciSequence

class Solver {
    fun fibonacciIndexOfFirstNumberLongerThan(digits: Int): Int {
        return infiniteFibonacciSequence(
                TruncatedInteger(1),
                TruncatedInteger(1),
                {a, b -> a + b}
        ).indexOfFirst { it.length >= digits} + 1
    }

}
