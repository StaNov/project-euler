package problem025

import kotlin.math.max

private const val KEEP_DIGITS = 5

class TruncatedInteger {

    private val truncatedValue: Long
    private val truncatedDigits: Int

    constructor(value: Long) : this(value.toString())

    constructor(value: String) : this(value, 0)

    private constructor(value: String, truncatedDigits: Int) {
        require(value.toLong() >= 0) { "No negative numbers" }
        this.truncatedValue = value.take(KEEP_DIGITS).toLong()
        this.truncatedDigits = value.drop(KEEP_DIGITS).length + truncatedDigits
    }

    operator fun plus(that: TruncatedInteger): TruncatedInteger {
        val maxLength = max(length, that.length)

        val thisValueTruncated = truncatedValue(maxLength)
        val thatValueTruncated = that.truncatedValue(maxLength)
        val resultTruncated = thisValueTruncated + thatValueTruncated

        return TruncatedInteger(
                resultTruncated.toString(),
                max(truncatedDigits, that.truncatedDigits)
        )
    }

    private fun truncatedValue(padToDigits: Int): Long {
        return (truncatedValue.toString() + "0".repeat(truncatedDigits))
                .padStart(padToDigits, '0')
                .take(KEEP_DIGITS)
                .toLong()
    }

    override fun toString(): String {
        var result = truncatedValue.toString()

        if (truncatedDigits > 0) {
            result += " * 10^$truncatedDigits"
        }

        return result
    }

    val length: Int
        get() {
            return truncatedValue.toString().length + truncatedDigits
        }
}