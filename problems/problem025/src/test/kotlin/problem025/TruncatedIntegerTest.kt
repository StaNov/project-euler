package problem025

import kotlin.test.Test
import kotlin.test.assertEquals

class TruncatedIntegerTest {

    @Test(expected = IllegalArgumentException::class)
    fun `No negative numbers`() {
        TruncatedInteger(-1)
    }

    @Test
    fun `Create non-truncated number`() {
        assertEquals("123", TruncatedInteger(123).toString())
    }

    @Test
    fun `Create truncated number`() {
        assertEquals("12345 * 10^2", TruncatedInteger(1234567).toString())
    }

    @Test
    fun `Add two numbers`() {
        assertEquals("3", TruncatedInteger(1).plus(TruncatedInteger(2)).toString())
    }

    @Test
    fun `Add two truncated numbers, same length, not over a next digit`() {
        assertEquals("30000 * 10^2", TruncatedInteger(10000_00).plus(TruncatedInteger(20000_00)).toString())
    }

    @Test
    fun `Add two truncated numbers, same length, over a next digit`() {
        assertEquals("12000 * 10^4", TruncatedInteger(90000_000).plus(TruncatedInteger(30000_000)).toString())
    }

    @Test
    fun `Add two truncated numbers, different length`() {
        assertEquals("11000 * 10^3", TruncatedInteger(10000_000).plus(TruncatedInteger(10000_00)).toString())
    }

    @Test
    fun `Add two truncated numbers, different length, other direction`() {
        assertEquals("11000 * 10^3", TruncatedInteger(10000_00).plus(TruncatedInteger(10000_000)).toString())
    }

    @Test
    fun `length, no truncated digits`() {
        assertEquals(4, TruncatedInteger(1234).length)
    }

    @Test
    fun `length, with truncated digits`() {
        assertEquals(9, TruncatedInteger(123456789).length)
    }
}