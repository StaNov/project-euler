package problem025

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun two() {
        assertEquals(7, solver.fibonacciIndexOfFirstNumberLongerThan(2))
    }

    @Test
    fun sample() {
        assertEquals(12, solver.fibonacciIndexOfFirstNumberLongerThan(3))
    }

    @Test
    fun full() {
        assertEquals(4782, solver.fibonacciIndexOfFirstNumberLongerThan(1000))
    }
}
