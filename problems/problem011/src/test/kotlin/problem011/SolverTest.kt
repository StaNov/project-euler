package problem011

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `Four numbers`() {
        assertEquals(180, solver.greatest4NumbersProductInGrid(
                """
                    03 03 04 05
                    02 02 02 02
                    02 02 02 02
                    02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Five numbers`() {
        assertEquals(360, solver.greatest4NumbersProductInGrid(
                """
                    02 03 04 05 06
                    02 02 02 02 02
                    02 02 02 02 02
                    02 02 02 02 02
                    02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `More numbers, in the middle`() {
        assertEquals(108, solver.greatest4NumbersProductInGrid(
                """
                    02 03 03 03 04 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Maximum is not on first line`() {
        assertEquals(7200, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02
                    02 02 02 02 02
                    02 20 04 30 03
                    02 02 02 02 02
                    02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `First column`() {
        assertEquals(240, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    03 02 02 02 02 02 02
                    04 02 02 02 02 02 02
                    04 02 02 02 02 02 02
                    05 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Another column`() {
        assertEquals(300, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 03 02 02 02
                    02 02 02 04 02 02 02
                    02 02 02 05 02 02 02
                    02 02 02 05 02 02 02
                    02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Down diagonal 1`() {
        assertEquals(480, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02
                    02 04 02 02 02
                    02 02 03 02 02
                    02 02 02 04 02
                    02 02 02 02 10
                """.trimIndent()))
    }

    @Test
    fun `Down diagonal 2`() {
        assertEquals(3000, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02 02
                    02 02 03 02 02 02 02
                    02 02 02 05 02 02 02
                    02 02 02 02 10 02 02
                    02 02 02 02 02 20 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Down diagonal 3`() {
        assertEquals(30000, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02
                    02 02 05 02 02 02
                    02 02 02 10 02 02
                    02 02 02 02 20 02
                    02 02 02 02 02 30
                    02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Down diagonal 4`() {
        assertEquals(720, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02
                    02 02 02 02 02 02
                    03 02 02 02 02 02
                    02 04 02 02 02 02
                    02 02 06 02 02 02
                    02 02 02 10 02 02
                """.trimIndent()))
    }

    @Test
    fun `Up diagonal 1`() {
        assertEquals(6000, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02
                    02 02 02 40 02
                    02 02 10 02 02
                    02 05 02 02 02
                    03 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Up diagonal 2`() {
        assertEquals(540, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02 02 02
                    02 02 02 02 09 02 02 02
                    02 02 02 04 02 02 02 02
                    02 02 03 02 02 02 02 02
                    02 05 02 02 02 02 02 02
                    02 02 02 02 02 02 02 02
                    02 02 02 02 02 02 02 02
                    02 02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun `Up diagonal 3`() {
        assertEquals(16000, solver.greatest4NumbersProductInGrid(
                """
                    02 02 02 02 02 02 02
                    02 02 02 02 02 02 02
                    02 02 02 02 02 40 02
                    02 02 02 02 20 02 02
                    02 02 02 05 02 02 02
                    02 02 04 02 02 02 02
                    02 02 02 02 02 02 02
                """.trimIndent()))
    }

    @Test
    fun full() {
        assertEquals(70600674, solver.greatest4NumbersProductInGrid())
    }
}
