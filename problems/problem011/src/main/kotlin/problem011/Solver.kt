package problem011

class Solver {
    fun greatest4NumbersProductInGrid(grid: String = FULL_GRID): Long {
        return createTuples(grid).map { it.fold(1L) { a, b -> a * b } }.max()!!
    }
}
