package problem011

fun createTuples(grid: String): List<List<Int>> {
    return TuplesCreator(grid).tuples
}

private class TuplesCreator(gridInput: String) {

    private val grid = gridInput
            .lines()
            .map {
                it.split(' ').map(String::toInt)
            }

    private val size = grid.size

    private val tuplesBuilder = mutableListOf<List<Int>>()

    val tuples: List<List<Int>>
        get() = tuplesBuilder.toList()

    init {
        require(grid.size == grid[0].size) { "Grid must be square." }
        generateLines()
        generateColumns()
        generateDownDiagonal()
        generateUpDiagonal()
    }

    private fun generateLines() {
        for (line in grid)
            addAllTuplesInLine(line)
    }

    private fun generateColumns() {
        for (columnIndex in 0 until size) {
            val column = IntRange(0, size - 1)
                    .map { lineIndex -> grid[lineIndex][columnIndex] }

            addAllTuplesInLine(column)
        }
    }

    private fun generateDownDiagonal() {

        generateDiagonal(-size until size) {
            lineIndexOfFirstColumn, columnIndex -> lineIndexOfFirstColumn + columnIndex
        }
    }

    private fun generateUpDiagonal() {
        generateDiagonal(0 until size * 2) {
            lineIndexOfFirstColumn, columnIndex -> lineIndexOfFirstColumn - columnIndex
        }
    }

    private fun generateDiagonal(
            lineIndexOfFirstColumnRange: IntRange,
            computeLineIndex: (Int, Int) -> Int
    ) {
        for (lineIndexOfFirstColumn in lineIndexOfFirstColumnRange)
            addAllTuplesInLine(createDiagonal(lineIndexOfFirstColumn, computeLineIndex))
    }

    private fun createDiagonal(
            lineIndexOfFirstColumn: Int,
            computeLineIndex: (Int, Int) -> Int
    ): List<Int> {
        val diagonal = mutableListOf<Int>()

        for (columnIndex in 0 until size) {
            val lineIndex = computeLineIndex(lineIndexOfFirstColumn, columnIndex)

            if (lineIndex in 0 until size)
                diagonal.add(grid[lineIndex][columnIndex])
        }

        return diagonal.toList()
    }

    private fun addAllTuplesInLine(line: List<Int>) {
        IntRange(0, line.size - 4)
                .map { startIndex -> line.subList(startIndex, startIndex + 4) }
                .forEach { tuplesBuilder.add(it) }
    }
}