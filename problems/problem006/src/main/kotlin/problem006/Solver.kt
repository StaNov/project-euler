package problem006

class Solver {
    fun differenceBetweenSumOfSquaresAndSquareOfSum(numbers: Int): Int {
        val sumOfSquares = (1..numbers).map { it * it }.sum()

        val sumOfNumbers = (1..numbers).sum()
        val squareOfSum = sumOfNumbers * sumOfNumbers

        return squareOfSum - sumOfSquares
    }

}
