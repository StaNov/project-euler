package problem006

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun sample() {
        assertEquals(2640, solver.differenceBetweenSumOfSquaresAndSquareOfSum(10))
    }

    @Test
    fun full() {
        assertEquals(25164150, solver.differenceBetweenSumOfSquaresAndSquareOfSum(100))
    }
}
