package templateRenameMe

// %%Uncomment%% import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun zero() {
        assertEquals(0, solver.sampleMethod(0))
    }

    @Test
    fun one() {
        assertEquals(1, solver.sampleMethod(1))
    }

    @Test
    // %%ToBeIgnored%%
    fun sample() {
        assertEquals(10, solver.sampleMethod(10))
    }

    @Test
    // %%ToBeIgnored%%
    fun full() {
        assertEquals(100, solver.sampleMethod(100))
    }
}
