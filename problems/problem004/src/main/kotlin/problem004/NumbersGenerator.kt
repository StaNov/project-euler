package problem004

import common.pow

internal fun generateNumbersOfDigits(digits: Int): List<Long> {
    val lowerBound = tenToPowerOf(digits - 1)
    val upperBound = tenToPowerOf(digits) - 1

    return (lowerBound..upperBound).toList()
}

private fun tenToPowerOf(exponent: Int): Long {
    return 10L.pow(exponent)
}
