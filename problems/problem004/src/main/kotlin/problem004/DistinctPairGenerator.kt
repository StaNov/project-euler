package problem004

import java.util.*

/**
 * Order is not guaranteed.
 */
fun <T> generateDistinctPairsOf(inputValues: List<T>): List<Pair<T, T>> {
    val result = mutableListOf<Pair<T, T>>()

    for (i in inputValues.indices)
        for (j in i until inputValues.size)
            result.add(Pair(inputValues[i], inputValues[j]))

    return Collections.unmodifiableList(result)
}