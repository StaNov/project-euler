package problem004

import palindromes.isPalindrome

class Solver {
    fun greatestPalindromeNumberProductOfDigitsNumbers(digits: Int): Long {
        val numbers = generateNumbersOfDigits(digits)
        val numberPairs = generateDistinctPairsOf(numbers)

        return numberPairs
                .map { it.first * it.second }
                .filter { isPalindrome(it) }
                .max()!!
    }

}
