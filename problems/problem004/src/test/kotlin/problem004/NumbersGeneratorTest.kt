package problem004

import kotlin.test.Test
import kotlin.test.assertEquals

class NumbersGeneratorTest {
    @Test
    fun `Generate one digit numbers`() {
        assertEquals((1L..9).toList(), generateNumbersOfDigits(1))
    }

    @Test
    fun `Generate two digit numbers`() {
        assertEquals((10L..99).toList(), generateNumbersOfDigits(2))
    }

    @Test
    fun `Generate three digit numbers`() {
        assertEquals((100L..999).toList(), generateNumbersOfDigits(3))
    }
}