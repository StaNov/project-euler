package problem004

import kotlin.test.Test
import kotlin.test.assertEquals

class DistinctPairGeneratorTest {
    @Test
    fun `No values, empty result`() {
        assertCorrectlyGenerated(listOf(), listOf<String>())
    }

    @Test
    fun `One value generates one pair`() {
        assertCorrectlyGenerated(listOf(Pair(1, 1)), listOf(1))
    }

    @Test
    fun `Two values generate three pairs`() {
        assertCorrectlyGenerated(listOf(Pair(1, 1), Pair(1, 2), Pair(2, 2)), listOf(1, 2))
    }

    @Test
    fun `Three values generate a lot of pairs`() {
        assertCorrectlyGenerated(listOf(
                Pair(1, 1),
                Pair(1, 2),
                Pair(1, 3),
                Pair(2, 2),
                Pair(2, 3),
                Pair(3, 3)
        ), listOf(1, 2, 3))
    }

    private fun <T> assertCorrectlyGenerated(expectedPairs: List<Pair<T, T>>, inputValues: List<T>) {
        assertEquals(expectedPairs.toSet(), generateDistinctPairsOf(inputValues).toSet())
    }
}