package problem004

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun one() {
        assertEquals(9, solver.greatestPalindromeNumberProductOfDigitsNumbers(1))
    }

    @Test
    fun sample() {
        assertEquals(9009, solver.greatestPalindromeNumberProductOfDigitsNumbers(2))
    }

    @Test
    fun full() {
        assertEquals(906609, solver.greatestPalindromeNumberProductOfDigitsNumbers(3))
    }
}
