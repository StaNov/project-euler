package problem016

import hugeint.HugeInt

class Solver {
    fun sumOfDigitsOfTwoToThePowerOf(exponent: Int): Int {
        var result = HugeInt(1)

        for (i in 1 .. exponent)
            result += result

        return result
                .toString()
                .map { it.toString().toInt() }
                .sum()
    }
}
