package problem016

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `2 ^ 0`() {
        assertSumOfDigits(1, 0)
    }

    @Test
    fun `2 ^ 1`() {
        assertSumOfDigits(2, 1)
    }

    @Test
    fun `2 ^ 2`() {
        assertSumOfDigits(4, 2)
    }

    @Test
    fun `2 ^ 4`() {
        assertSumOfDigits(7, 4)
    }

    @Test
    fun sample() {
        assertSumOfDigits(26, 15)
    }

    @Test
    fun full() {
        assertSumOfDigits(1366, 1000)
    }

    private fun assertSumOfDigits(expectedDigitSum: Int, exponent: Int) {
        assertEquals(expectedDigitSum, solver.sumOfDigitsOfTwoToThePowerOf(exponent))
    }
}
