package problem014

import minmax.KeyValueMinMaxKeeper

class Solver {
    fun numberWithLongestCollatzSequenceBelow(upperBound: Int): Int {
        val keeper = KeyValueMinMaxKeeper<Int, Int>()

        for (i in 1 until upperBound) {
            keeper.add(i, collatzSequenceLengthStartingWith(i))
        }

        return keeper.maxKey
    }
}
