package problem014

fun collatzSequenceStartingWith(startingNumber: Int) : List<Long> {
    val result = mutableListOf<Long>()
    result.add(startingNumber.toLong())

    var currentNumber = startingNumber.toLong()
    while (currentNumber != 1L) {
        currentNumber = determineNextNumber(currentNumber)
        result.add(currentNumber)
    }

    return result.toList()
}

fun collatzSequenceLengthStartingWith(startingNumber: Int) : Int {
    var result = 1 // starting number

    var currentNumber = startingNumber.toLong()
    while (currentNumber != 1L) {
        currentNumber = determineNextNumber(currentNumber)
        result += 1
    }

    return result
}

private fun determineNextNumber(currentNumber: Long): Long {
    return if (currentNumber % 2 == 0L)
        currentNumber / 2
    else
        currentNumber * 3 + 1
}