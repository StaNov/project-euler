package problem014

import kotlin.test.Test
import kotlin.test.assertEquals

class CollatzSequenceGeneratorTest {

    @Test
    fun `Collatz one`() {
        assertCorrectCollatzSequence(1, listOf(1))
    }

    @Test
    fun `Collatz two`() {
        assertCorrectCollatzSequence(2, listOf(2, 1))
    }

    @Test
    fun `Collatz three`() {
        assertCorrectCollatzSequence(3, listOf(3, 10, 5, 16, 8, 4, 2, 1))
    }

    @Test
    fun `Collatz thirteen`() {
        assertCorrectCollatzSequence(13, listOf(13, 40, 20, 10, 5, 16, 8, 4, 2, 1))
    }

    @Test
    fun `Length function gives the same results`() {
        for (i in 1 .. 1000)
            assertEquals(collatzSequenceLengthStartingWith(i), collatzSequenceStartingWith(i).size)
    }

    private fun assertCorrectCollatzSequence(startingNumber: Int, expected: List<Long>) {
        assertEquals(expected, collatzSequenceStartingWith(startingNumber))
    }
}