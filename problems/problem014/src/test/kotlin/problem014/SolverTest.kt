package problem014

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `Under 2, there is only one`() {
        assertEquals(1, solver.numberWithLongestCollatzSequenceBelow(2))
    }

    @Test
    fun `Under 3, two is longer`() {
        assertEquals(2, solver.numberWithLongestCollatzSequenceBelow(3))
    }

    @Test
    fun `Under 5, three is longest`() {
        assertEquals(3, solver.numberWithLongestCollatzSequenceBelow(5))
    }

    @Test
    fun sample() {
        assertEquals(9, solver.numberWithLongestCollatzSequenceBelow(14))
    }

    @Test
    fun full() {
        assertEquals(837799, solver.numberWithLongestCollatzSequenceBelow(1_000_000))
    }
}
