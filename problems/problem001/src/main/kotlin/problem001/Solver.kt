package problem001

class Solver {
    fun sumOfMultiplesOf3Or5Below(below: Int): Int {
        return IntRange(0, below - 1)
                .filter { it % 3 == 0 || it % 5 == 0 }
                .sum()
    }

}
