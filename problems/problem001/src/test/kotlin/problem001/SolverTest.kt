package problem001

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    @Test
    fun zero() {
        val solver = Solver()
        assertEquals(0, solver.sumOfMultiplesOf3Or5Below(0))
    }

    @Test
    fun four() {
        val solver = Solver()
        assertEquals(3, solver.sumOfMultiplesOf3Or5Below(4))
    }

    @Test
    fun six() {
        val solver = Solver()
        assertEquals(8, solver.sumOfMultiplesOf3Or5Below(6))
    }

    @Test
    fun sample() {
        val solver = Solver()
        assertEquals(23, solver.sumOfMultiplesOf3Or5Below(10))
    }

    @Test
    fun full() {
        val solver = Solver()
        assertEquals(233168, solver.sumOfMultiplesOf3Or5Below(1000))
    }
}
