package problem013

import hugeint.HugeInt

class Solver {
    fun sumOfNumbersFirstTenDigits(numbers: String = PRODUCTION_NUMBERS): String {
        return numbers
                .lines()
                .map { HugeInt(it) }
                .fold(HugeInt(0)) {a, b -> a + b}
                .toString()
                .take(10)
    }
}
