package problem013

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun `One number`() {
        assertEquals("1234567890", solver.sumOfNumbersFirstTenDigits("1234567890"))
    }

    @Test
    fun `One long number`() {
        assertEquals("1234567890", solver.sumOfNumbersFirstTenDigits("123456789011111111"))
    }

    @Test
    fun sample() {
        assertEquals("8348422521", solver.sumOfNumbersFirstTenDigits("""
            37107287533902102798797998220837590246510135740250
            46376937677490009712648124896970078050417018260538
        """.trimIndent()))
    }

    @Test
    fun full() {
        assertEquals("5537376230", solver.sumOfNumbersFirstTenDigits())
    }
}
