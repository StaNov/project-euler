package problem009

fun isPythagoreanTriplet(triplet: Triple<Int, Int, Int>): Boolean {
    val (a, b, c) = triplet.toList().sorted()
    return a*a + b*b == c*c
}