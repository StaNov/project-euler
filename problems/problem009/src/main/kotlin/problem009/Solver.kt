package problem009

class Solver {
    fun productOfPythagoreanTripletWithSum(sum: Int): Int {
        val triplet = tripletsWithSum(sum)
                .find { isPythagoreanTriplet(it) }
                ?: throw IllegalStateException("A triplet was not found.")

        return triplet.first * triplet.second * triplet.third
    }
}
