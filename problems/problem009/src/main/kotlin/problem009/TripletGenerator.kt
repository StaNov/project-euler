package problem009

import kotlin.math.ceil

fun tripletsWithSum(sum: Int): Sequence<Triple<Int, Int, Int>> {
    if (sum < 6) {
        return emptySequence()
    }

    return sequence {
        for (first in 1 until sum/3)
            for (second in first+1 until ceil((sum-first)/2.0).toInt())
                yield(Triple(first, second, sum - first - second))
    }
}