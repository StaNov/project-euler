package problem009

import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    private val solver: Solver = Solver()

    @Test
    fun sample() {
        assertEquals(3 * 4 * 5, solver.productOfPythagoreanTripletWithSum(3 + 4 + 5))
    }

    @Test
    fun full() {
        assertEquals(31875000, solver.productOfPythagoreanTripletWithSum(1000))
    }
}
