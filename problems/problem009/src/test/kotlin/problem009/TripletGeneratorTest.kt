package problem009

import kotlin.test.Test
import kotlin.test.assertEquals

class TripletGeneratorTest {
    @Test
    fun `There is no triplet with sum less than six`() {
        assertTriplets(listOf(), 5)
    }

    @Test
    fun `Smallest triplet`() {
        assertTriplets(listOf(Triple(1, 2, 3)), 6)
    }

    @Test
    fun `Seven triplet`() {
        assertTriplets(listOf(Triple(1, 2, 4)), 7)
    }

    @Test
    fun `Eight triplets`() {
        assertTriplets(listOf(
                Triple(1, 2, 5),
                Triple(1, 3, 4)
        ), 8)
    }

    @Test
    fun `Nine triplets`() {
        assertTriplets(listOf(
                Triple(1, 2, 6),
                Triple(1, 3, 5),
                Triple(2, 3, 4)
        ), 9)
    }

    private fun assertTriplets(expected: List<Triple<Int, Int, Int>>, sum: Int) {
        assertEquals(expected, tripletsWithSum(sum).toList())
    }
}