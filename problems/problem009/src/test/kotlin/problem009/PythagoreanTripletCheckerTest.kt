package problem009

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PythagoreanTripletCheckerTest {
    @Test
    fun `Not a triplet`() {
        assertFalse(isPythagoreanTriplet(Triple(1, 2, 3)))
    }

    @Test
    fun `Yes a triplet`() {
        assertTrue(isPythagoreanTriplet(Triple(3, 4, 5)))
    }
}